#!/bin/fish

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# Present dmenu asking to install or remove the scripts to user scripts path
echo -e "Add\nRemove" | $DMENU_CMD -i -l 10 -p "Add or remove scripts?" | read CHOICE

#User path location where symbolic links are created
set USER_SCRIPTS_PATH ~/.local/bin/

#List of available scripts
set SCRIPTS "apps" "monitor" "system" "browser" "lastpass" "projects" "media" "vpn" "shortcuts" "communication"

switch $CHOICE
case Add
    echo "Addings symlinks to user scripts path $USER_SCRIPTS_PATH"

    for SCRIPT in $SCRIPTS
        #Check if link is already present and skip if that is true
        if test -e "$USER_SCRIPTS_PATH/$SCRIPT.fish"
            echo "Script already present."
        else
            ln -rs $SCRIPT.fish $USER_SCRIPTS_PATH
            echo "Created symlink to $SCRIPT.fish"
        end
    end

case Remove
    echo -e "Removing symlinks from $USER_SCRIPTS_PATH"

    for SCRIPT in $SCRIPTS
        #Check if link is present and remove if that is true
        if test -e "$USER_SCRIPTS_PATH/$SCRIPT.fish"
            rm "$USER_SCRIPTS_PATH/$SCRIPT.fish"
            echo "Removed symlink to $SCRIPT.fish"
        else
            echo "Script not found."
        end
    end

case '*'
    echo "Unknown selection."
end
