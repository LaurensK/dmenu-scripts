#!/bin/fish

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the $TERMINAL_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase $TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# Present dmenu with list of communication tools and read selected choice
echo -e "1. Rambox\n2. Matterhorn\n3. Mail" | $DMENU_CMD -i -l 20 -p "Start communication tool" | read CHOICE

switch $CHOICE
  case "1*"
		rambox

  case "2*"
		$TERMINAL -e matterhorn

	case "3*"
		# Change to downloads for folder for default attachment save location
		cd /home/laurens/Downloads
    $TERMINAL -e neomutt

  case '*'
    echo "Unknown selection."
end
