#!/bin/fish
# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# Present dmenu with list of leave options and read selected choice
echo -e "Check for updates\nhtop\nnload\nsensors\n\n1. Lock\n2. Suspend\n3. Shutdown\n4. Reboot\n5. Logout" | $DMENU_CMD -i -l 10 -p "Leave" | read CHOICE

switch $CHOICE
case "*updates*"
    cd /home/laurens/projects/dmenu-scripts
    $TERMINAL -e ./pamac_wrapper.fish

case "htop"
    $TERMINAL -e htop

case "nload"
    $TERMINAL -e nload wlp0s20f3

case "sensors"
    cd /home/laurens/projects/dmenu-scripts
    $TERMINAL -e ./sensors_wrapper.fish

case "1*"
    echo Locking
    i3exit lock
case "2*"
    echo Suspending
    i3exit suspend
case "3*"
    echo Shutting down now
    $TERMINAL -e shutdown now
case "4*"
    echo Going for reboot now
    $TERMINAL -e reboot
case "5*"
    echo Loggin off from i3
    i3exit logout
case '*'
    echo "Unknown selection."
end
