#!/bin/fish

#### Audio control ####
# 1. Toggle output (speakers)
#pulseaudio-ctl mute
# 2. Toggle input source (microfoon)
#pulseaudio-ctl mute-input
# 3. Volume up/down
#pulseaudio-ctl up 5
#pulseaudio-ctl down 5
# 4.1 Volume 20%
#pulseaudio-ctl set 20
# 4.2 Volume 50%
#pulseaudio-ctl set 50
# 4.3 Volume 80%
#pulseaudio-ctl set 80


# Mount remote music collection via SSHFS to default music dir(Mpd uses this) if not already done so
#set MUSIC_DIR (xdg-user-dir MUSIC)
#
#if test (ls -A $MUSIC_DIR | count) -eq 0
#    echo "$MUSIC_DIR is empty, attempting to mount using sshfs"
#    # 'lakohost-remote' is set in ssh config
#    set REMOTE_MUSIC_DIR lakohost-remote:/srv/storage/music
#    sshfs $REMOTE_MUSIC_DIR $MUSIC_DIR
#    if test (ls -A $MUSIC_DIR | count) -gt 0
#        echo "$REMOTE_MUSIC_DIR mounted successfully."
#
#also start mpDris2 to use media button and playerctl configured in i3-config
#        # MPD is configured to start as soon as its socket is triggered:
#        # "systemctl enable --user mpd.socket"
#systemctl start --user mpDris2.service
#
#        #update the database
#        mpc update
#    end
#end

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the $TERMINAL_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase $TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# Present dmenu with list of mpd commands
# then read selected account into choice
set COMMAND_LIST "Toggle" "Next" "Previous" "Browse library" "Volume 20%" "Volume 30%" "Volume 50%" "Volume 100%"
echo -e (string join "\n" $COMMAND_LIST) | $DMENU_CMD -i -l 20 -p "Music control:" | read CHOICE

switch $CHOICE
case Toggle
    playerctl play-pause

case Next
    playerctl next

case Previous
    playerctl previous

case (string match 'Browse*' $CHOICE)
    echo "Browse library"
    $TERMINAL -e "ncmpcpp"

case "Volume 20%"
    playerctl volume 0.2

case "Volume 30%"
    playerctl volume 0.3

case "Volume 50%"
    playerctl volume 0.5

case "Volume 100%"
    playerctl volume 1.0

case '*'
    set PLAY_STATUS (ncmpcpp --current-song)
    notify-send "$PLAY_STATUS"

end
