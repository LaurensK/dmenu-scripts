#!/bin/fish
pamac checkupdates -a | while read -L PAMAC_LINE
  echo $PAMAC_LINE
  set -a -g PAMAC_RESULT $PAMAC_LINE
end

for PAMAC_LINE in $PAMAC_RESULT
  if string match -q '*available update*' $PAMAC_LINE
    set -g NUM_UPDATES (string split ' ' $PAMAC_LINE)[1]
  end
  if string match -q '*up-to-date*' $PAMAC_LINE
    set -g NUM_UPDATES 0
  end
end

if test $NUM_UPDATES -eq 0
  read -P "There are no updates. Press enter to exit."
  exit
else
  read -g -n 1 -P "There are $NUM_UPDATES updates. Do you want to upgrade? (y/n) " BLN_UPGRADE 
end

if test $BLN_UPGRADE = 'y'
  pamac upgrade -a
else
  echo "Decided not to upgrade!"
end
