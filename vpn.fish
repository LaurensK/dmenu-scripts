#!/bin/fish
# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# Present dmenu with available vpn's
set VPN_LIST "1. minienw up" "2. overheid-sp up" " " "3. minienw down" "4. overheid-sp down"
echo -e (string join "\n" $VPN_LIST) | $DMENU_CMD -i -l 20 -p "Select VPN:" | read CHOICE

switch $CHOICE
case "1*"
  nmcli connection up minienm_openlev

case "2*"
  nmcli connection up overheid-sp

case "3*"
  nmcli connection down minienm_openlev

case "4*"
  nmcli connection down overheid-sp

end
