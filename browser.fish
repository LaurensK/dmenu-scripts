#!/bin/fish

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# Present dmenu with list of browser and read selected choice
echo -e "1. Qutebrowser(Default)\n2. Firefox\n3. Chromium\n4. W3m" | $DMENU_CMD -i -l 20 -p "Start browser" | read CHOICE

set FF_PROFILES "werk\nprive\nfinancieel"
set QB_SESSIONS "prive\nwerk\nnew\nsecure"

switch $CHOICE
  case "1*"
    echo -e $QB_SESSIONS | $DMENU_CMD -i -l 10 -p "Select Qutebrowser session" | read SESSION

    set QT_FLAGS --qt-flag ignore-gpu-blacklist --qt-flag enable-accelerated-video --qt-flag enable-gpu-rasterization --qt-flag enable-oop-rasterization --qt-flag enable-hardware-overlays --qt-flag enable-native-gpu-memory-buffers --qt-flag num-raster-threads=4

   switch $SESSION
      case "p*"
          qutebrowser $QT_FLAGS -r prive &

      case "w*"
          qutebrowser $QT_FLAGS -r werk &

      case "n*"
          qutebrowser $QT_FLAGS -R &

      case "s*"
          qutebrowser $QT_FLAGS --temp-basedir --set content.private_browsing true &

      case '*'
          echo Opening current session
          qutebrowser $QT_FLAGS &
      end

  case "2*"
    echo -e $FF_PROFILES | $DMENU_CMD -i -l 10 -p "Select Firefox profile" | read SELECTED_PROFILE

    # Opens Profile manager if nothing is selected
    firefox -P "laurens-"$SELECTED_PROFILE &

  case "3*"
    chromium &

  case "4*"
    echo -n | $DMENU_CMD -p "Enter url:" | read URL
    $TERMINAL -e w3m $URL &

  case '*'
    echo "Unknown selection."
end
