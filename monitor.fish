#!/bin/fish
# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

#Get the connected external outputs
set connectedOutputs (xrandr | grep -i " connected" | sed -e "s/\([A-Z0-9]\+\) connected.*/\1/")

#Start arandr if  more then 1 external monitor is detected
if test (count $connectedOutputs) -gt 2
  arandr &
  exit
end

#If only internal output is available present a simpler menu
if test (count $connectedOutputs) -eq 1
  echo -e "1. Rerun Xrandr\n2. Open Arandr\n3. Source Xprofile" | $DMENU_CMD -i -l 3 -p "Only Laptop monitor found. Select action:" | read CHOICE

  switch $CHOICE
    case "1*"
      xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal

    case "2*"
      arandr

    case "3*"
      source ~/.xprofile

    case '*'
      echo "Unknown selection."
  end

  exit
else
# Present dmenu with list of monitor configurations and read selected choice
  echo -e "1. Laptop\n2. Duplicate\n3. External\n4. ExternalRight\n5. ExternalAbove\n6. Custom" | $DMENU_CMD -i -l 10 -p "Monitor" | read CHOICE

  switch $CHOICE
  case "1*"
      echo "Selecting internal monitor."
      xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal \
        --output $connectedOutputs[2] --off

  case "2*"
      echo "Selecting duplicate output on internal monitor and external output"
      xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal \
        --output $connectedOutputs[2] --auto --primary --pos 0x0 --rotate normal

  case "3*"
      echo "Selecting external output"
      xrandr --output eDP-1 --off \
        --output $connectedOutputs[2] --auto --primary --pos 0x0 --rotate normal

  case "4*"
      echo "Selecting output on internal monitor left and external output right"
      xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal \
        --output $connectedOutputs[2] --auto --primary --pos 1920x0 --rotate normal

  case "5*"
      echo "Selecting output on internal monitor bottom and external output above"
      xrandr --output eDP-1 --mode 1920x1080 --pos 0x1080 --rotate normal \
        --output $connectedOutputs[2] --auto --primary --pos 0x0 --rotate normal

  case "6*"
      echo "Select custom monitor setup with arandr"
      arandr

  case '*'
      echo "Unknown selection."
  end

#Always reload .xprofile settings after monitor change
  source ~/.xprofile
end
