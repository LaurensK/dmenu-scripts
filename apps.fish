#!/bin/fish
# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

set APPS "0. Vifm" "1. Communication" "2. Browser" "3. Intellij" "4. Postman" "5. JMeter" "6. Vimwiki" \
  "7. PCmanFm(Filemanager)" "8. Pamac-manager" "9. Newsboat RSS"

# Present dmenu with list of apps and read selected choice
echo -e (string join "\n" $APPS) | $DMENU_CMD -i -l 20 -p "Start application" | read CHOICE

switch $CHOICE
  case "0*"
    $TERMINAL -e vifm

  case "1*"
    communication.fish

  case "2*"
    browser.fish

  case "3*"
		idea

  case "4*"
    postman

  case "5*"
    jmeter

  case "6*"
		vimwiki

  case "7*"
    pcmanfm

  case "8*"
    pamac-manager

  case "9*"
    $TERMINAL -e newsboat -r

case '*'
    exit 0
end
