A set of convenience scripts in [Fish-shell](https://fishshell.com). Quickly start an app, shortcut,
use Lastpass-cli, leave, set monitor outputs or interact with MPD.

Menu's are built with as little keystrokes in mind as possible. This can be achieved by using them
with [dmenu-instant](https://gitlab.com/LaurensK/dmenu-instant).

[[_TOC_]]

## Scripts summary

* **install** - Quickly add/remove symlinks to user path location to execute a script
* **apps** - Start an available application
* **browser** - Start an available browser
* **lastpass** - Copy username or password, generate new or open vault from your lastpass account
* **leave** - Shutdown or reboot computer. Suspend, lock or exit i3 (Script uses i3exit and systemctl)
* **monitor** - Select monitor(s)-configuration and positions using xrandr/arandr
* **projects** - Start working on a projects (usually opening a terminal with Nvim-session)
* **music** - [MPD](https://wiki.archlinux.org/index.php/Music_Player_Daemon) player controls,
Pavucontrol quick mute toggles
* **vpn** - Activate a vpn connection using nmcli
* **shortcuts** - Open images folders with Sxiv

## Usage

Make sure your dmenu is built with the '-n' instant option. Otherwise remove the '-n' from all DMENU_CMD
variables.

These scripts are designed to be run from a keybinding.

i3 Example:

```sh
bindsym $mod+F1 exec --no-startup-id apps
```

## Add or remove scripts from your PATH

1. Clone the repository
1. Configure value of USER_SCRIPTS_PATH in script 'install'
1. Run from repository location:

```fish
./install
```

This will spawn a dmenu that gives you the option to add or remove the scripts from the USER_SCRIPTS_PATH

## Run a script

Run in a terminal:

```fish
SCRIPTNAME
```

Where SCRIPTNAME is the name of one of the current available scripts ( ex: 'monitor' ).
This will spawn a dmenu where you can select and activate an action.

## Apps

TODO

## Shortcuts (TODO: rename projects?)

1. Pictures
   1. Wallpapers
   1. Random saved pictures
   1. All
   1. Fotos on server
1. Documents
1. Work TFG
1. Work RWS
   1. PR13-Scripts
   1. PR13-GitlabCE-docker-deployment
   1. PR13-GitlabEE-dso-keten
   1. PR13-GitlabEE-toepasbare-regels
   1. PR13-GitlabEE-toepasbare-regels-test
   1. PR13-GitlabEE-docker-config
   1. PR13-GitlabEE-swagger
   1. TFG RWS Uren
   1. TFG RWS Certificaten
