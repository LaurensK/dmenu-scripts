#!/bin/fish

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the $TERMINAL_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase $TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

set PROJECT_GROUPS "Configs" "Projects" "RWS"
set PROJECTS_CONFIG   "Nvim" "i3" "Vifm" "Newsboat" "Alacritty" "MPD"
set PROJECTS_RWS "toepasbare-regels-test" "toepasbare-regels"
set PROJECTS_OTHER "dmenu-scripts" "dmenu-instant" "SSH Lakohost"

# Present dmenu with list of project groups
echo -e (string join "\n" $PROJECT_GROUPS) | $DMENU_CMD -i -l 10 -p "Select project group:" | read SELECTED_PROJECT_GROUP

switch $SELECTED_PROJECT_GROUP
  case "Co*"
    set PROJECTS $PROJECTS_CONFIG

  case "Pr*"
    set PROJECTS $PROJECTS_OTHER

  case "RWS"
    set PROJECTS $PROJECTS_RWS

end

# Present dmenu with list of projects and read selected choice
echo -e (string join "\n" $PROJECTS) | $DMENU_CMD -i -l 10 -p "Start project" | read CHOICE

switch $CHOICE
#PROJECTS_CONFIG
case "Nvim"
    cd ~/.config/nvim
    $TERMINAL -e nvim -S

case "i3"
    cd ~/.config
    $TERMINAL -e nvim -p ./i3/config ./i3status/config

case "Vifm"
    cd ~/.config/vifm
    $TERMINAL -e nvim -p vifmrc vifminfo vifminfo_2819

case "Newsboat"
    cd ~/.config/newsboat
    $TERMINAL -e nvim -p config urls

case "Alacritty"
    cd ~/.config/alacritty
    $TERMINAL -e nvim alacritty.yml

case "MPD"
    cd ~/.config/mpd
    $TERMINAL -e nvim mpd.conf

#PROJECTS_OTHER
case "dmenu-scripts"
    cd ~/projects/dmenu-scripts
    $TERMINAL -e nvim -p *.fish

case "dmenu-instant"
    cd ~/projects/dmenu-instant
    $TERMINAL -e nvim -S

case "SSH L*"
    $TERMINAL -e ssh lakohost

#PROJECTS_RWS
case "toepasbare-regels-test"
    cd ~/PR13/gitlabEE/toepasbare-regels-test
    $TERMINAL -e nvim

case "toepasbare-regels"
    cd ~/PR13/gitlabEE/toepasbare-regels
    $TERMINAL -e nvim

end
