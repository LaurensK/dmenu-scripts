#!/bin/fish

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

set IMG_VIEWER "sxiv -tr"
set SC_PICTURES "1. Wallpapers" "2. Saved Pictures" "3. All local" "4. Photos on lakohost" "\n"
set SC_DOCUMENTS "5. Documents"
# Present dmenu with list of project groups
echo -e (string join "\n" $SC_PICTURES) | $DMENU_CMD -i -l 10 -p "Select shortcut:" | read SELECTED_SC

switch $SELECTED_SC
  case "1.*"
    eval $IMG_VIEWER "~/Pictures/wallpapers"

  case "2.*"
    eval $IMG_VIEWER "~/Pictures/opgeslagen-fotos"

  case "3.*"
    cd ~/Pictures
    set FOLDERS (ls --group-directories-first)
    echo -e (string join "\n" $FOLDERS) | $DMENU_CMD -i -l 40 -p "Select folder or image:" | read SELECT
    # Appending pwd enables thumbnail viewer with selected image highlighted
    eval $IMG_VIEWER $SELECT (pwd)

  case "4.*"
    set BASE_DIR /mnt/lakohost/photos
    set BASE_FOLDERS (ls $BASE_DIR)

    echo -e (string join "\n" $FOLDERS) | $DMENU_CMD -i -l 40 -p "Select folder:" | read SELECT
end
