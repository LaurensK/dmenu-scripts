#!/bin/fish

# Mount remote music collection via SSHFS to default music dir(Mpd uses this) if not already done so
# set MUSIC_DIR (xdg-user-dir MUSIC)
# set REMOTE_MUSIC_DIR /mnt/storage/music

#also start mpDris2 to use media button and playerctl configured in i3-config
#        # MPD is configured to start as soon as its socket is triggered:
#        # "systemctl enable --user mpd.socket"
# systemctl start --user mpDris2.service

#if test (ls -A $MUSIC_DIR | count) -eq 0
#  echo "$MUSIC_DIR is empty - looking for external disk"
#    if test (ls -A $REMOTE_MUSIC_DIR | count) -eq 0
#      echo "$REMOTE_MUSIC_DIR is empty - no music available on system"
#    else
#      #mount the remote music dir to xdg-music-dir, this way mpd config does not need to be changed
#      bindfs -r $REMOTE_MUSIC_DIR $MUSIC_DIR
#    end
#end

#if not test (ls -A $MUSIC_DIR | count) -eq 0
#  echo "$REMOTE_MUSIC_DIR mounted successfully."
#  #update database if music is present
#  mpc update
#end

#    echo "$MUSIC_DIR is empty, attempting to mount using sshfs"
#    # 'lakohost-remote' is set in ssh config
#    set REMOTE_MUSIC_DIR lakohost-remote:/srv/storage/music
#    sshfs $REMOTE_MUSIC_DIR $MUSIC_DIR
#    if test (ls -A $MUSIC_DIR | count) -gt 0
#        echo "$REMOTE_MUSIC_DIR mounted successfully."
#
#
#        #update the database
#        mpc update
#    end

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the $TERMINAL_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase $TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

set PA_CMDS "Audio control:" " 1. Toggle output (speakers)" " 2. Toggle input source (microfoon)" " 3. Volume up/down" " 4. Volume set"
set MPD_CMDS " " "Music player daemon control:" " 1. Toggle play/pause" " 2. Next" " 3. Previous" " 4. Browse library" " " "Music player volume control:" " Volume 20%" " Volume 30%" " Volume 50%" " Volume 100%"
echo -e (string join "\n" $PA_CMDS $MPD_CMDS ) | $DMENU_CMD -i -l 20 -p "Media control:" | read CHOICE

switch $CHOICE
case "*1. Toggle output*"
    pulseaudio-ctl mute

case "*2. Toggle input*"
    pulseaudio-ctl mute-input

case "*3. Volume*"
    set VOL_UPDOWN "down" "up"
    set DO 1
    while test $DO = 1
        echo -e (string join "\n" $VOL_UPDOWN) | $DMENU_CMD -i -l 2 -p "Volume up/down 5%:" | read VOL_ADJUST
        if string match -r "up|down" $VOL_ADJUST
            pulseaudio-ctl $VOL_ADJUST 5
        else
            break
        end
    end

case "*4. Volume set"
    set FIXED_VOLUME_PERC "20" "50" "80"
    echo -e (string join "\n" $FIXED_VOLUME_PERC) | $DMENU_CMD -i -l 3 -p "Volume set:" | read VOL_ADJUST
    pulseaudio-ctl set $VOL_ADJUST

case "*1. Toggle play*"
    playerctl play-pause

case "*2. Next"
    playerctl next

case "*3. Previous"
    playerctl previous

case "*4. Browse lib*"
    echo "Browse library"
    $TERMINAL -e "ncmpcpp"

case "*Volume 20%"
    playerctl volume 0.2

case "*Volume 30%"
    playerctl volume 0.3

case "*Volume 50%"
    playerctl volume 0.5

case "*Volume 100%"
    playerctl volume 1.0

case '*'
    set PLAY_STATUS (ncmpcpp --current-song)
    notify-send "$PLAY_STATUS"

end
