#!/bin/fish

# Copy default config of dmenu to home if it doesn't exist
if not test -e "$HOME/.dmenurc"
	cp /usr/share/dmenu/dmenurc $HOME/.dmenurc
end

# parse each line from .dmenurc, convert to fish and evaluate
for line in (grep "=" ~/.dmenurc)
    for i in (string split "=" $line)
        #echo "Found i value: $i"
       set -a cmd $i
    end
    eval set $cmd
    set --erase cmd
end

# evaluating the terminal_cmd is not needed and using the dmenu-options line from .dmenurc does not work due to quotes, so need to construct that here
set --erase TERMINAL_CMD
set --erase DMENU_OPTIONS

# base dmenu command
set DMENU_CMD dmenu -n -fn $DMENU_FN -nb $DMENU_NB -nf $DMENU_NF -sb $DMENU_SB -sf $DMENU_SF

# check if e-mail is known, ask if not
if not test -e ~/.cache/.lp_dmenu_email
    echo -n | $DMENU_CMD -p "Enter E-Mail for your LastPass:" | read EMAIL
    echo -n $EMAIL > ~/.cache/.lp_dmenu_email
end

# login if not logged in
if test (lpass status) = 'Not logged in.'
    echo "Requesting to log in"
    lpass login (cat ~/.cache/.lp_dmenu_email)
    lpass ls > /dev/null
end

# Present dmenu with list of lastpass commands and the vault,
# then read selected account into choice
set LPASS_LIST "0. Generate new" "1. Open vault\n" (lpass ls --format "%an" --sync=no --color=never)
echo -e (string join "\n" $LPASS_LIST) | $DMENU_CMD -i -l 20 -p "Lastpass site:" | read CHOICE

# Exit if no correct selection
if test -z $CHOICE
    echo "Unknown selection."
    exit
end

# Execute new account dialog sequence if Generate new is selected
if string match '*Generate new*' $CHOICE
    echo "Generate pasword"

    # Generate and clip a password and store it under account name temp, without a username or a url
    lpass generate -c temp 12

    exit
end

# Open Vault in Qutebrowser if so requested
if string match '*Open vault*' $CHOICE
    echo "Opening Lastpass Vault with browser"

    qutebrowser "https://lastpass.com/?ac=1&lpnorefresh=1"

    exit
end

# Copy username or password to clipboard
echo -e "Password(Default)\nUsername" | $DMENU_CMD -i -p "Copy to clipboard:" | read REQUEST

set COPY "--password"
if test -n $REQUEST -a $REQUEST = "Username"
    set COPY "--username"
end

# Execute lpass show
echo "Requesting account for $CHOICE"
lpass show --sync=no -c "$COPY" "$CHOICE"

# notify if no entry was entered or found
if test $status -gt 0
    notify-send "No entry found for: $CHOICE"
end
